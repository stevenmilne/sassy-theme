import React from 'react'
import classNames from 'classnames'
import { Theme } from 'sassy-theme/theme'
import './Panel.scss'

export interface PanelProps {
  title: string
  theme?: Theme
  className?: string
  contentClassName?: string
}

const Panel: React.FunctionComponent<PanelProps> = ({ title, theme, className, contentClassName, children }) => (
  <div className={classNames('panel', theme, className)}>
    <div className="panel__header">
      {title}
    </div>
    <div className={classNames('panel__content', contentClassName)}>
      {children}
    </div>
  </div>
)

export default Panel