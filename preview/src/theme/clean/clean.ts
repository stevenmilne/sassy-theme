import { ThemeSet } from 'sassy-theme/theme'

export const ThemeClean: ThemeSet = {
  DEFAULT: 'theme__clean',
  DARK: 'theme__clean--dark'
}
