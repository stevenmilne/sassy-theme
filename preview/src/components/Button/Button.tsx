import React from 'react'
import classNames from 'classnames'
import { Theme } from 'sassy-theme/theme'
import './Button.scss'

export interface ButtonProps {
  theme?: Theme
  className?: string
  onClick?: () => void
}

const Button: React.FunctionComponent<ButtonProps> = ({ theme, className, onClick, children }) => (
  <button className={classNames('button', theme, className)} onClick={onClick}>
    {children}
  </button>
)

export default Button