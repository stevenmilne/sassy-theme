import React from 'react';
import classNames from 'classnames'
import { ThemeClean } from '../../theme/clean/clean';
import Panel from '../Panel/Panel'
import '../../theme/clean/clean.scss'
import './App.scss'
import Button from '../Button/Button';
import Select from '../Select/Select';
import { Theme } from 'sassy-theme/theme';
import Input from '../Input/Input';

export interface AppState {
  theme: Theme
}

const THEMES = [
  { id: ThemeClean.DEFAULT, name: 'Clean' },
  { id: ThemeClean.DARK, name: 'Clean - Dark' }
]

export default class App extends React.Component<{}, AppState> {
  public state: AppState = {
    theme: ThemeClean.DEFAULT
  }

  public render() {
    const { theme } = this.state

    return (
      <div className={classNames('app', [ThemeClean.DEFAULT, theme])}>
        <Panel title="Sassy Theme Preview" className="app__panel" contentClassName="app__panel-content">
          <Button onClick={this.handleLightTheme} theme={ThemeClean.DEFAULT}>Light</Button>
          <Button onClick={this.handleDarkTheme} theme={ThemeClean.DARK}>Dark</Button>
          <Select options={THEMES} onChange={this.handleThemeChange} />
          <Input name="thing" />
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique magna et faucibus malesuada. In euismod sagittis lorem, nec porttitor metus ornare a. Integer maximus lacus eros, quis feugiat urna luctus eget. Aenean eu purus mi. Vestibulum interdum lorem ac diam pretium feugiat. Nulla mattis sollicitudin erat, non lobortis felis dictum scelerisque. Curabitur convallis diam massa, et sodales eros imperdiet at. Aliquam sit amet tempus ligula. Vestibulum scelerisque tempus neque sed commodo. In eu libero ac tortor elementum laoreet. Sed non congue mi, cursus porta turpis.
          </p>
          <p>
            Quisque varius mauris nunc, nec egestas nunc gravida sit amet. Aliquam non egestas sem. Duis dapibus vestibulum convallis. Sed condimentum, est a molestie consequat, nibh nunc efficitur lorem, eu tempor mauris ex non neque. Aenean congue feugiat iaculis. Donec pharetra dui ante, eu egestas eros semper nec. In tempor accumsan metus nec lacinia. Suspendisse justo mi, bibendum nec dolor rhoncus, ultrices commodo quam. Nam id interdum ante. Quisque libero lacus, accumsan vel vehicula eu, pharetra at elit. Mauris sagittis, nibh eu viverra scelerisque, tortor ligula ultricies metus, eget maximus dui dolor malesuada ipsum. Suspendisse euismod feugiat bibendum.
          </p>
          <p>
            Nam vulputate nunc purus, nec vehicula augue molestie et. Cras faucibus turpis sed metus porttitor, sed elementum augue efficitur. Curabitur elementum, nisl sit amet faucibus pellentesque, quam lacus hendrerit tellus, a ullamcorper nulla orci vitae mi. Vivamus non ullamcorper enim. Etiam eu dapibus nulla. Maecenas at rutrum lectus, bibendum ultrices nisi. Integer tincidunt diam pretium nisl gravida vestibulum a lobortis odio. Etiam congue, nunc id pulvinar pellentesque, felis neque hendrerit nisi, vitae commodo est dui ac risus. In posuere convallis purus, ut congue metus rhoncus at. Pellentesque accumsan tincidunt mauris eu faucibus.
          </p>
          <p>
            Morbi fringilla quis justo ac volutpat. Curabitur facilisis tempus tristique. Mauris massa erat, lobortis ut ipsum varius, faucibus finibus erat. Donec dui eros, interdum a rhoncus sit amet, hendrerit eget purus. Cras eget lectus dui. Phasellus tempus purus sapien, id vehicula felis auctor eu. Sed varius mollis lorem sit amet efficitur. Donec convallis at ligula in tincidunt. Donec velit odio, ultrices feugiat sem sed, porttitor tempus nisi. Phasellus ac tellus ac nisi pulvinar eleifend. Ut maximus faucibus nulla non condimentum.
          </p>
          <p>
            Donec id elementum nisi. Donec eu nunc felis. Suspendisse a arcu quis lacus ultricies tempus non at felis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. In gravida semper velit eget fermentum. Maecenas interdum dapibus hendrerit. Maecenas interdum, dui luctus pretium ornare, ipsum tortor consequat urna, id iaculis ligula massa vitae ipsum. Donec ut lacus dolor. Maecenas dapibus pretium quam vel mattis. Quisque dapibus, nibh nec lobortis fermentum, justo erat porta lorem, in sodales nisl turpis ut leo. Nulla facilisi.
          </p>
        </Panel>
      </div>
    )
  }

  private handleThemeChange = (value: string) => this.setState({ theme: value })

  private handleLightTheme = () => this.setState({ theme: ThemeClean.DEFAULT })

  private handleDarkTheme = () => this.setState({ theme: ThemeClean.DARK })
}
