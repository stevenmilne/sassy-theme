import { Flavor } from "./@types/flavor"

export type ThemeClass = Flavor<string, "ThemeClass">

export interface ThemeSet {
  [index: string]: ThemeClass
}

export type Theme = ThemeClass | ThemeClass[]
