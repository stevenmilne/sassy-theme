# Sassy Theme - Preview
This package contains a React app which allows you to preview and test [sassy-theme](https://gitlab.com/stevenmilne/sassy-themehttps://gitlab.com/stevenmilne/sassy-theme).

## Usage
Run the following command to launch the preview tool;

> `npm start`

Once it is running you should be able to navigate your web browser to http://localhost:3000.
