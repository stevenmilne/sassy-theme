# Sassy Theme
A [SASS](https://sass-lang.com/) interface which makes [React](https://reactjs.org/) components more portable while still being super easy to theme. The interface has a collection of properties and helper mixins that a theme can implement and provide at runtime. Using the interface you can easily mix and match themes at runtime by just changing a CSS class.

## Demo
Included within this repository is a demo app which shows how to use the interface and shows off the advantages it brings to the table. The demo app is included in the `preview` directory. You can run the preview app locally with the command;

> `npm run preview`

## Usage
The interface is exposed as a SASS module, to use it just import it;

```scss
@use '~sassy-theme/theme.scss';

.button {
  @include theme.text();
  @include theme.bordered();

  transition: background-color 250ms;
  background-color: theme.$primary-colour;
  color: theme.$font-colour-primary;

  &:hover,
  &:focus,
  &:active {
    background-color: theme.$primary-colour-shade;
  }
}
```

The example above shows how you could style a button component with theme properties and helpers.

## License
MIT License

Copyright (c) 2020 Steven Ian Milne

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.