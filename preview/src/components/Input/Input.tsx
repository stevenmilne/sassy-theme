import React from 'react'
import classNames from 'classnames'
import { Theme } from 'sassy-theme/theme'
import './Input.scss'

export interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  className?: string
  theme?: Theme
}

const Input: React.FunctionComponent<InputProps> = ({ theme, className, ...others }) => (
  <input className={classNames('input', theme, className)} {...others} />
)

export default Input