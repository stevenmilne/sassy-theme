import React from 'react'
import classNames from 'classnames'
import { Theme } from 'sassy-theme/theme'
import './Select.scss'

export interface Option {
  id: string
  name: string
}

export interface SelectProps {
  onChange(value: string): void
  options: Option[]
  value?: string
  theme?: Theme
  className?: string
}

export interface SelectState {
  value: string
}

export default class Select extends React.Component<SelectProps, SelectState> {
  public state: SelectState = {
    value: ''
  }

  public render() {
    const { theme, className, options } = this.props
    const { value } = this.state

    return (
      <div className={classNames('select', theme, className)}>
        <select className="select__field" value={value} onChange={this.handleChange}>
          {options.map(option =>
            <option key={option.id} value={option.id}>{option.name}</option>
          )}
        </select>
        <div className="select__arrow" />
      </div>
    )
  }

  private handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { onChange } = this.props
    const value = event.target.value

    this.setState({ value })
    onChange(value)
  }
}